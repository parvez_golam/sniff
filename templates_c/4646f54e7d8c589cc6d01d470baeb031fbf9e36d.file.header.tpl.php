<?php /* Smarty version Smarty-3.1.7, created on 2012-05-11 15:26:12
         compiled from "/var/www/sniff/templates/header.tpl" */ ?>
<?php /*%%SmartyHeaderCode:4886387974f11f611ba0601-51718779%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4646f54e7d8c589cc6d01d470baeb031fbf9e36d' => 
    array (
      0 => '/var/www/sniff/templates/header.tpl',
      1 => 1336746235,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '4886387974f11f611ba0601-51718779',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.7',
  'unifunc' => 'content_4f11f611c7788',
  'variables' => 
  array (
    'global_url' => 0,
    'league' => 0,
    'loggedin' => 0,
    'month_ids' => 0,
    'month_names' => 0,
    'selected' => 0,
    'data' => 0,
    'td_val' => 0,
    'username' => 0,
    'c_data' => 0,
    'alert_msg' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_4f11f611c7788')) {function content_4f11f611c7788($_smarty_tpl) {?><?php if (!is_callable('smarty_function_html_options')) include '/var/www/sniff/libs/plugins/function.html_options.php';
if (!is_callable('smarty_function_html_table')) include '/var/www/sniff/libs/plugins/function.html_table.php';
?><!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>BlisMobile Media</title>
<!--[if lte IE 7]>
<style>
.content { margin-right: -1px; } 
ul.nav a { zoom: 1; }  
</style>
<![endif]-->
<link href="<?php echo $_smarty_tpl->tpl_vars['global_url']->value;?>
css/blis_followon_black.css" rel="stylesheet" type="text/css">
<script type="text/javascript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
</script>

<?php if ($_smarty_tpl->tpl_vars['league']->value!="yes"){?>
	<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;sensor=false&amp;key=ABQIAAAA4GSdBzM5YuIN3Ldq625S8BQdOMrJiztTnmlCALsj6NajD9tHxhQmJE96rUeaCLmq39pBufa6XB15LQ" type="text/javascript"></script>

	<script type="text/javascript">
	var map;
	var mapCenter
	var geocoder;
	var fakeLatitude;
	var fakeLongitude;

	function initialize() 
	{
		if (navigator.geolocation)
		{
			navigator.geolocation.getCurrentPosition( function (position) {
				mapThisGoogle(position.coords.latitude,position.coords.longitude);
	
				s = position.coords.latitude+","+position.coords.longitude;
				document.forms[0].q.value = s;
			},
			function (error)
			{
				switch(error.code)
				{
					case error.PERMISSION_DENIED:
						document.forms[0].btn_login.disabled=true;
						break;
				}
			}
			);
		}
		else
		{
			document.forms[0].btn_login.disabled=true;
		}
	}

	function mapThisGoogle(latitude,longitude)
	{
		var mapCenter = new GLatLng(latitude,longitude);
		map = new GMap2(document.getElementById("map_canvas"));
		map.setCenter(mapCenter, 15);
		map.addOverlay(new GMarker(mapCenter));

		geocoder = new GClientGeocoder();
		geocoder.getLocations(latitude+','+longitude, addAddressToMap);
	}

	function addAddressToMap(response)
	{
		if (!response || response.Status.code != 200)
		{
			alert("Sorry, we were unable to geocode that address");
		}
		else
		{
			place = response.Placemark[0];
			var addr = document.getElementById('address');
			addr.firstChild.data = 'Your address : '+place.address;
		}
	}
	</script>

	<?php if ($_smarty_tpl->tpl_vars['loggedin']->value=="yes"){?>
	<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['global_url']->value;?>
/js/balloon.config.js"></script>
	<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['global_url']->value;?>
/js/balloon.js"></script>
	<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['global_url']->value;?>
/js/box.js"></script>
	<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['global_url']->value;?>
/js/yahoo-dom-event.js"></script>

	<script type="text/javascript">
		var balloon = new Balloon;
		var tooltip = new Balloon;
		BalloonConfig(tooltip,'GPlain');
	</script>

	<script src="<?php echo $_smarty_tpl->tpl_vars['global_url']->value;?>
/js/gen_validatorv4.js" type="text/javascript"></script>
	<?php }?>
<?php }?>

</head>
<?php if ($_smarty_tpl->tpl_vars['league']->value!="yes"){?>
<body onload="setTimeout('initialize()',500);">
<?php }else{ ?>
<body>
<?php }?>
<div class="container">  <div class="content">
	<div id="topnavarea">
		<ul>
			<li><a href="http://blismobile.com/index.html"><img src="<?php echo $_smarty_tpl->tpl_vars['global_url']->value;?>
images/home_topnav.png" width="41" height="30" alt="Home"></a></li>
			<li class="what"><a href="http://blismobile.com/who_we_are.html"><img src="<?php echo $_smarty_tpl->tpl_vars['global_url']->value;?>
images/grey_whoarewe_topnav.png" width="86" height="30" alt="Blis mobile Who are we"></a></li>
			<li><a href="http://blismobile.com/blis_products.html"><img src="<?php echo $_smarty_tpl->tpl_vars['global_url']->value;?>
images/greyproducts_topnav.png" width="62" height="30" alt="Products"></a></li>
			<li><a href="http://blismobile.com/blis_analytics.html"><img src="<?php echo $_smarty_tpl->tpl_vars['global_url']->value;?>
images/grey_analytics_topnav.png" width="65" height="30" alt="BlisMobile Media Analytics"></a></li>
			<li><a href="http://blismobile.com/meet_clients.html"><img src="<?php echo $_smarty_tpl->tpl_vars['global_url']->value;?>
images/meet_topnav.png" width="110" height="30" alt="Meet our clients"></a></li>
			<li><a href="http://blismobile.com/have_coffee_board.html"><img src="<?php echo $_smarty_tpl->tpl_vars['global_url']->value;?>
images/coffee_topnav.png" width="127" height="30" alt="Let's have a coffee"></a></li>
		</ul>
	</div>

	<div class="main">
		<div class="subtitle">
			<?php if ($_smarty_tpl->tpl_vars['league']->value=="yes"){?>
				<center>TOP 10 USERS FOR<br/>
					<form action="" method="POST">
						<select name="s_months" onchange='this.form.submit()'>
							<?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['month_ids']->value,'output'=>$_smarty_tpl->tpl_vars['month_names']->value,'selected'=>$_smarty_tpl->tpl_vars['selected']->value),$_smarty_tpl);?>

						</select>
					</form>
				</center>
				<?php echo smarty_function_html_table(array('loop'=>$_smarty_tpl->tpl_vars['data']->value,'cols'=>"Username, Amount",'table_attr'=>'align="center", cellpadding="3", cellspacing="3"','td_attr'=>$_smarty_tpl->tpl_vars['td_val']->value),$_smarty_tpl);?>

			<?php }else{ ?>
				<?php if ($_smarty_tpl->tpl_vars['loggedin']->value!="yes"){?>
					<form action="<?php echo $_smarty_tpl->tpl_vars['global_url']->value;?>
index.php" method="POST">
						Username : <input type="text" name="login" value="<?php echo $_POST['login'];?>
">
						<input type="submit" value="Login" name="btn_login">
					</form>
				<?php }else{ ?>
					<span style="font-size:12px;">Logged as <a href="<?php echo $_smarty_tpl->tpl_vars['global_url']->value;?>
index.php?do=logout&user=<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['username']->value;?>
</a></span><br/>
					<span style="font-size:12px;color:red;">You will be kept logged in until you log out (click your name above to log out). If you are on a public computer you should log out, otherwise you can stay logged in</span><br/><br/>
					<form action="<?php echo $_smarty_tpl->tpl_vars['global_url']->value;?>
index.php" method="POST" enctype="multipart/form-data" name="grab_loc">
						<span style="font-size:12px;">
							WiFi Network Name (SSID) : <input type="text" name="ssid" onmouseover="balloon.showTooltip(event,'Please enter the name of the WiFi network that you are connected to (SSID), for example AT&T or BTOpenzone.',0,300)"><br/>
							Location name : <input type="text" name="location" onmouseover="balloon.showTooltip(event,'For example BA Lounge Heathrow Airport Terminal 3, or Starbucks Coffee.',0,300)"><br/>
							Location description : <input type="text" name="location_descr" onmouseover="balloon.showTooltip(event,'Please enter the type of location e.g. Hotel, Coffee Shop, Airport, Shopping Mall etc.',0,300)"><br/>
							Country : <?php echo smarty_function_html_options(array('name'=>"country",'output'=>$_smarty_tpl->tpl_vars['c_data']->value,'values'=>$_smarty_tpl->tpl_vars['c_data']->value),$_smarty_tpl);?>
<br/>
							Coordinates : <input type="text" name="q" value="">&nbsp;&nbsp;&nbsp;<a href="#" onClick="window.location.reload()">Refresh Location</a><br/>
							<span style="font-size:12px;color:red;"><?php echo $_smarty_tpl->tpl_vars['alert_msg']->value;?>
</span><br/>
							<input type="submit" value="Grab!"><br/><br/>
							<div id="map_canvas" style="width: 500px; height: 300px"></div>
							<p id="address">You location is: Unknown.</p>
						</span>
	
						<script  type="text/javascript">
							var frmvalidator = new Validator("grab_loc");
							frmvalidator.EnableMsgsTogether();

							frmvalidator.addValidation("ssid", "req", "Please ensure SSID field is completed");
							frmvalidator.addValidation("location", "req", "Please ensure Location field is completed");
							frmvalidator.addValidation("location_descr", "req", "Please ensure Location Description field is completed");
						</script>
					</form><br/>
				<?php }?>
			<?php }?>
		</div>
	</div>
</div>
<?php }} ?>